<?php

foreach(glob('./source/*.xml') as $files){
	$counter +=1;
	list($root,$folder,$item) = explode('/',$files);
	$xml = simplexml_load_file($files);
	$domtree = new DOMDocument('1.0', 'UTF-8');
	$domtree->xmlStandalone = true;
	$domtree->formatOutput = true;
	$Doklad = $domtree->createElement("Doklad");
	$Doklad = $domtree->appendChild($Doklad);

	foreach($xml as $data){
		$Dodavatel = $domtree->createElement('Dodavatel',$data->Supplier->OrgIdentNumber);
		$Dodavatel = $Doklad->appendChild($Dodavatel);
		$D_nazev = $domtree->createElement('D_nazev',$data->Supplier->Name);
		$D_nazev = $Doklad->appendChild($D_nazev);
		$Ico = $domtree->createElement('ICO',$data->Supplier->OrgIdentNumber);
		$Ico = $Doklad->appendChild($Ico);
		$Dic = $domtree->createElement('DIC',$data->Supplier->VATOrgIdentNumber);
		$Dic = $Doklad->appendChild($Dic);
		$Faktura = $domtree->createElement('Faktura',$data['docname']);
		$Faktura = $Doklad->appendChild($Faktura);
		$Variabilni_symbol = $domtree->createElement('VS',$data->VarSymbol);
		$Variabilni_symbol = $Doklad->appendChild($Variabilni_symbol);
		$Vystavena = $domtree->createElement('Vystavena',$data->VATDate);
		$Vystavena = $Doklad->appendChild($Vystavena);
		$Duzp = $domtree->createElement('DUZP',$data->VATDate);
		$Duzp = $Doklad->appendChild($Duzp);
		$Splatnost = $domtree->createElement('Splatnost',$data->DueDate);
		$Splatnost = $Doklad->appendChild($Splatnost);
		$Celkem = $domtree->createElement('Celkem',$data->Amount);
		$Celkem = $Doklad->appendChild($Celkem);
		$Zaklad_0 = $domtree->createElement('Zaklad_0',0);
		$Zaklad_0 = $Doklad->appendChild($Zaklad_0);
		$Zaklad_1 = $domtree->createElement('Zaklad_1',$data->AmountWithoutVAT);
		$Zaklad_1 = $Doklad->appendChild($Zaklad_1);
		$Dph_1 = $domtree->createElement('Dph_1',($data->Amount-$data->AmountWithoutVAT));
		$Dph_1 = $Doklad->appendChild($Dph_1);
		$Zaklad_2 = $domtree->createElement('Zaklad_2',0);
		$Zaklad_2 = $Doklad->appendChild($Zaklad_2);
		$Dph_2 = $domtree->createElement('Dph_2',0);
		$Dph_2 = $Doklad->appendChild($Dph_2);
		$Zaklad_3 = $domtree->createElement('Zaklad_3',0);
		$Zaklad_3 = $Doklad->appendChild($Zaklad_3);
		$Dph_3 = $domtree->createElement('Dph_3',0);
		$Dph_3 = $Doklad->appendChild($Dph_3);
		$Objednavka = $domtree->createElement('Objednavka','');
		$Objednavka = $Doklad->appendChild($Objednavka);
		$Polozky = $domtree->createElement('Polozky');
		$Polozky = $Doklad->appendChild($Polozky);

		foreach($data->Rows->Row as $radek){
			$Polozka = $domtree->createElement('Polozka');
			$Polozka = $Polozky->appendChild($Polozka);
			$Nazev = $domtree->createElement('Nazev', trim($radek->Commodity->Name));
			$Nazev = $Polozka->appendChild($Nazev);
			$Kod_zbozi = $domtree->createElement('Kod_zbozi', $radek->Commodity->CustomerCode);
			$Kod_zbozi = $Polozka->appendChild($Kod_zbozi);
			$Dod_kod = $domtree->createElement('Dod_kod', $radek->Commodity->SupplierCode);
			$Dod_kod = $Polozka->appendChild($Dod_kod);
			$Ean = $domtree->createElement('Ean', $radek->Commodity->EAN);
			$Ean = $Polozka->appendChild($Ean);
			$Mnozstvi = $domtree->createElement('Mnozstvi', $radek->Quantity);
			$Mnozstvi = $Polozka->appendChild($Mnozstvi);
			$Cena_j = $domtree->createElement('Cena_j', str_replace(',','.',$radek->UnitPrice));
			$Cena_j = $Polozka->appendChild($Cena_j);
			$Sazba_DPH = $domtree->createElement('Sazba_DPH', $radek->VATRate);
			$Sazba_DPH = $Polozka->appendChild($Sazba_DPH);
		}
		
	$domtree->save('./output/'.$data['id'].'.xml');	
	}
}
echo '<h4>Počet překonvertovaných souborů: '.$counter.'</h4>';


